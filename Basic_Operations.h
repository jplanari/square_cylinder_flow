double StepCalculation(int i, double* x_grid)
{
	return x_grid[i] - x_grid[i-1];
}

double Size2Norm(double A, double B)
{
	return sqrt(A*A + B*B);
}

double SetTimestep(int Nx, int Ny, double** u, double** v, double* x_grid, double* y_grid, double Re)
{
	double xstep, ystep;
	double Dx,U;
	double tc = 5e9;
	double td = 5e9;

	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			xstep = StepCalculation(i,x_grid);
			ystep = StepCalculation(j,y_grid);
			Dx = Size2Norm(xstep,ystep);
			U = Size2Norm(u[i][j],v[i][j]);
			if(0.35*Dx/U < tc)
			{
				tc = 0.35*Dx/U;
			}

			if(0.20*Re*xstep*ystep < td)
			{
				td = 0.20*Re*xstep*ystep;
			}
		}
	}

	return min(tc,td);
}

void MATRIX_VECTOR_SPARSE(int xlim, int ylim, disc_coefficients** A, double* x, double Prod[])
{
	int k = 0;
	int jump = ylim-1;
	for(int i = 1; i < xlim; i++)
	{
		for (int j = 1; j < ylim; j++)
		{
			if(j == 1)
			{
				if(i == 1)
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aE*x[k+jump] - A[i][j].aN*x[k+1];
				}
				else if (i == xlim-1)
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aW*x[k-jump] - A[i][j].aN*x[k+1];
				}
				else
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aN*x[k+1] - A[i][j].aE*x[k+jump] - A[i][j].aW*x[k-jump];
				}
			}
			else if(j == ylim-1)
			{
				if(i == 1)
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aE*x[k+jump] - A[i][j].aS*x[k-1];
				}
				else if (i == xlim-1)
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aW*x[k-jump] - A[i][j].aS*x[k-1];
				}
				else
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aS*x[k-1] - A[i][j].aE*x[k+jump] - A[i][j].aW*x[k-jump];
				}
			}
			else
			{
				if(i == 1)
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aE*x[k+jump] - A[i][j].aN*x[k+1] - A[i][j].aS*x[k-1];
				}
				else if (i == xlim-1)
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aW*x[k-jump] - A[i][j].aN*x[k+1] - A[i][j].aS*x[k-1];
				}
				else
				{
					Prod[k] = A[i][j].aP*x[k] - A[i][j].aN*x[k+1] - A[i][j].aE*x[k+jump] - A[i][j].aW*x[k-jump] - A[i][j].aS*x[k-1];
				}
			}
			k++;
		}
	}
}



double SCALAR_PROD(double A[], double B[], int lim)
{
	double sum = 0;
	for(int i = 0; i < lim; i++)
	{
		sum += A[i]*B[i];
	}
	return sum;
}

void DIF_VECTOR(double A[], double B[], double C[], double fac, int lim)
{
	for(int i = 0; i < lim; i++)
	{
		A[i] = B[i] + fac*C[i];
	}
}

void ConjugateGradientMethod(int xlim, int ylim, disc_coefficients** A, double** Initial_Guess, double** sol, double tolerance)
{
	double B[xlim*ylim];
	double Ax0[xlim*ylim];
	double r[xlim*ylim];
	double p[xlim*ylim];
	double alph, beta;
	double rsq;
	double solvec[xlim*ylim];
	double veclim = (xlim-2)*(ylim-2);

	//Construct independent term vector
	//cout << "B:" << endl;
	int l = 0;
	for(int i = 1; i < xlim-1; i++)
	{
		for (int j = 1; j < ylim-1; j++)
		{
			B[l] = A[i][j].bP;
			l++;
		}
	}

	l = 0;
	for(int i = 1; i < xlim-1; i++)
	{
		for (int j = 1; j < ylim-1; j++)
		{
			solvec[l] = Initial_Guess[i][j];
			l++;
		}
	}

	MATRIX_VECTOR_SPARSE(xlim-1,ylim-1,A,solvec,Ax0);

	DIF_VECTOR(r,B,Ax0,-1,veclim);

	if(sqrt(SCALAR_PROD(r,r,veclim)) > tolerance)
	{
		DIF_VECTOR(p,r,r,0,veclim);
		while(sqrt(SCALAR_PROD(r,r,veclim)) > tolerance)
		{

			MATRIX_VECTOR_SPARSE(xlim-1,ylim-1,A,p,Ax0);
			alph = SCALAR_PROD(r,r,veclim)/SCALAR_PROD(p,Ax0,veclim);
			rsq = SCALAR_PROD(r,r,veclim);
			DIF_VECTOR(solvec,solvec,p,alph,veclim);

			DIF_VECTOR(r,r,Ax0,-1*alph,veclim);

			if(sqrt(SCALAR_PROD(r,r,veclim)) < tolerance)
			{
				break;
			}
			else
			{
				beta = SCALAR_PROD(r,r,veclim)/rsq;
				DIF_VECTOR(p,r,p,beta,veclim);
			}
		}
	}

	int k = 0;
	for(int i = 1; i < xlim-1; i++)
	{
		for (int j = 1; j < ylim-1; j++)
		{
			sol[i][j] = solvec[k];
			k++;
		}
	}
}

double Gradient(int i, int j, double** p, double* x, bool direction)
{
	double dplus, dmin;
	double pres_plus, pres_min;
	if(direction)
	{
		dplus = x[i+1]-x[i];
		dmin = x[i]-x[i-1];

		pres_plus = p[i+1][j] - p[i][j];
		pres_min = p[i][j] - p[i-1][j];
	}
	else
	{
		dplus = x[j+1]-x[j];
		dmin = x[j]-x[j-1];

		pres_plus = p[i][j+1]-p[i][j];
		pres_min = p[i][j] - p[i][j-1];
	}

	return 0.5*(pres_plus*dmin+pres_min*dplus)/(dplus*dmin);
}

void CalculateVorticity(int Nx, int Ny, double** u, double** v, double* x, double* y, double** w)
{
	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			w[i][j] = Gradient(i,j,v,x,true) - Gradient(i,j,u,y,false);
		}
	}
}

void StreamFunctions(int Nx, int Ny, double* x_grid, double* y_grid, double** u, double** v, double** phi)
{
	double dx, dy;
	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			dx = StepCalculation(i,x_grid);
			dy = StepCalculation(j,y_grid);
			phi[i][j] = u[i][j]*dy-v[i][j]*dx;
		}
	}
}
