#include <iostream>
#include <cmath>
#include <ctime>
#include <stdlib.h>
#include <cassert>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>

using namespace std;

struct disc_coefficients
{
	double aE, aW, aN, aS, aP, bP;
};

struct mass_flow
{
	double me, mw, mn, ms;
};

struct cylinder
{
	double size;
	double center_x, center_y;
	int i0, i1, j0, j1;
	double x0, x1, y0, y1;
};

#include "Basic_Operations.h"
#include "Previous_Calculations.h"
#include "Pressure.h"
#include "FractionalStepMethod.h"


int main(int argc, char** argv)
{
	if(argc != 7)
	{
		cout << "Please input correctly the parameters." << endl;
		cout << "COMPILED_NAME Re Nx Ny Lx Ly ratio" << endl;
		return 1;
	}

	// INPUT PARAMETERS READING

	double Re = atof(argv[1]);
	int Nx = atoi(argv[2]);
	int Ny = atoi(argv[3]);
	double Lx = atof(argv[4]);
	double Ly = atof(argv[5]);

	double ratio = atof(argv[6]);

	cylinder C;

	// CONVERGENCE RESIDUALS

	double time_tolerance = 1e-7;
	double pressure_tolerance = 1e-10;

	// VARIABLE DEFINITIONS

	double** Ru = new double*[Nx+2];
	double** Rv = new double*[Nx+2];
	double** p = new double*[Nx+2];
	double** u = new double*[Nx+2];
	double** v = new double*[Nx+2];
	double** up = new double*[Nx+2];
	double** vp = new double*[Nx+2];
	disc_coefficients** Pres = new disc_coefficients*[Nx+2];
	mass_flow** m_dot = new mass_flow*[Nx+2];
	double** vort = new double*[Nx+2];
	double** phi = new double*[Nx+2];

	double* x = new double[Nx+2];
	double* x_grid = new double[Nx+1];

	double* y = new double[Ny+2];
	double* y_grid = new double[Ny+1];

	for(int i = 0; i < Nx+2; i++)
	{
		Ru[i] = new double[Ny+2];
		Rv[i] = new double[Ny+2];
		p[i] = new double[Ny+2];
		u[i] = new double[Ny+2];
		v[i] = new double[Ny+2];
		up[i] = new double[Ny+2];
		vp[i] = new double[Ny+2];
		vort[i] = new double[Ny+2];
		Pres[i] = new disc_coefficients[Ny+2];
		m_dot[i] = new mass_flow[Ny+2];
		phi[i] = new double[Ny+2];
	}

	string fullpath;
	string path = "/home/jplanari/Documents/CTTC-HT/NAVIER_STOKES/07_SQUARE_CYLINDER";

	double velocity_inlet = 1.0;
	double pressure_inlet = 1.0;
	double pressure_outlet = 0.0;
	double pressure_mid = 0.5*(pressure_inlet+pressure_outlet);

	// MESH GENERATION, independent for x and y

	GridGeneration(Lx,Nx,x,x_grid,true);
	GridGeneration(Ly,Ny,y,y_grid,false);

	FillCylinder(C,Nx,Ny,Lx,Ly,x,y,ratio);

	SetInitialConditions(Nx,Ny,u,0.0,0.0,velocity_inlet,0.0,0.0);
	SetInitialConditions(Nx,Ny,v,0.0,0.0,0.0,0.0,0.0);
	SetPressure(Nx,Ny,p,x,pressure_inlet,pressure_outlet);

	PressureMatrixCoefficients(Nx,Ny,x,y,x_grid,y_grid,Pres);

	double time_error = 100;
	double timestep = SetTimestep(Nx,Ny,u,v,x_grid,y_grid,Re);
	double time = 0.0;

	double ystep;

	MassFlowCalculations(Nx,Ny,C,u,v,p,x,y,x_grid,y_grid,timestep,m_dot,velocity_inlet);

	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			if(i>C.i0 && i<C.i1 && j>C.j0 && j<C.j1)
			{
				Ru[i][j] = 0.0;
				Rv[i][j] = 0.0;
			}
			else
			{
				Ru[i][j] = CalculateR(i,j,Ny,C,u,m_dot,x_grid,y_grid,x,y,Re,velocity_inlet,true);
				Rv[i][j] = CalculateR(i,j,Ny,C,u,m_dot,x_grid,y_grid,x,y,Re,velocity_inlet,false);
			}
		}
	}

	int k = 0;
	int file = 0;

	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "CHANNEL FLOW PROBLEM. Re = " << Re << ", N = " << Nx*Ny << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

	clock_t t;

	double time_lim = 600;

	while(time_error > time_tolerance && time < time_lim)
	{
		time_error = (FractionalStepMethod(Nx,Ny,C,Re,timestep,pressure_tolerance,u,v,up,vp,p,pressure_inlet,pressure_outlet,velocity_inlet,Ru,Rv,m_dot,Pres,x,y,x_grid,y_grid))/timestep;

		MassFlowCalculations(Nx,Ny,C,u,v,p,x,y,x_grid,y_grid,timestep,m_dot,velocity_inlet);

		CalculateVorticity(Nx,Ny,u,v,x,y,vort);

		StreamFunctions(Nx,Ny,x_grid,y_grid,u,v,phi);

		if(k%10==0)
		{
				cout << "Time: " << time << "; error = " << time_error << endl;
		}

		if(k%50 == 0)
		{
				ostringstream ost;
				ost << file;

				string title = ost.str();

				fullpath = path + "/vid/channel.csv." + title;

				ofstream U_file(fullpath);

				if(U_file.is_open())
				{
					U_file << "x coord, y coord, z coord, u, v, p, vort, stre" << endl;
					for(int j = 0; j < Ny+2; j++)
					{
						for(int i = 0; i < Nx+2; i++)
						{
							U_file << x[i] << ", " << y[j] << ", 0, " << u[i][j] << ", " << v[i][j] << ", " << p[i][j] << ", " << vort[i][j] << ", " << phi[i][j] << endl;
						}
					}
					U_file.close();
				}
				file++;
		}

		time += timestep;

		timestep = SetTimestep(Nx,Ny,u,v,x_grid,y_grid,Re);
		k++;
	}

	t = clock() - t;

	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	cout << "SIMULATION FINISHED with Re = " << Re << " in " << (float)t/CLOCKS_PER_SEC << " s." << endl;
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;



	ofstream res("Results.dat");
	if (res.is_open())
	{
		for (int i = 0; i < Nx+2; i++)
		{
			for (int j = 0; j < Ny+2; j++)
			{
				res << x[i] << " " << y[j] << " " << u[i][j] << " " << v[i][j] << " " << p[i][j] << endl;
			}
			res << endl;
		}
		res.close();
	}

	ofstream res_para("Results.csv");
	if(res_para.is_open())
	{
		res_para << "x coord, y coord, z coord, u, v, p, vort, stre" << endl;
		for(int j = 0; j < Ny+2; j++)
		{
			for(int i = 0; i < Nx+2; i++)
			{
				res_para << x[i] << ", " << y[j] << ", 0, " << u[i][j] << ", " << v[i][j] << ", " << p[i][j] << ", " << vort[i][j] << ", " << phi[i][j] << endl;
			}
		}
		res_para.close();
	}

	for(int i = 0; i < Nx+2; i++)
	{
		delete [] Ru[i];
		delete [] Rv[i];
		delete [] p[i];
		delete [] u[i];
		delete [] v[i];
		delete [] up[i];
		delete [] vp[i];
		delete [] Pres[i];
		delete [] m_dot[i];
		delete [] vort[i];
		delete [] phi[i];
	}

	delete [] Ru;
	delete [] Rv;
	delete [] p;
	delete [] u;
	delete [] v;
	delete [] up;
	delete [] vp;
	delete [] Pres;
	delete [] m_dot;
	delete [] x;
	delete [] x_grid;
	delete [] y;
	delete [] y_grid;
	delete [] vort;
	delete [] phi;



	return 0;



}
