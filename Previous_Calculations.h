void NonUniformDiscretization(double L, int N, double* x, double* x_grid)
{
	for(int i = 0; i < N + 1; i++)
	{
		x_grid[i] = L*0.5*(1 - cos(M_PI*(double)(i)/(double)(N)));
	}

	x[0] = 0.0;
	x[N+1] = L;
	for(int i = 1; i < N + 1; i++)
	{
		x[i] = 0.5*(x_grid[i]+x_grid[i-1]);
	}
}

void UniformDiscretization(double L, int N, double* x, double* x_grid)
{
	double delta = L/(double)(N);

	for(int i = 0; i < N + 1; i++)
	{
		x_grid[i] = i*delta;
	}

	x[0] = 0.0;
	x[N+1] = L;
	for(int i = 1; i < N + 1; i++)
	{
		x[i] = 0.5*(x_grid[i]+x_grid[i-1]);
	}
}

void GridGeneration(double L, int N, double* x, double* x_grid, bool uniform)
{
	if(uniform)
	{
		UniformDiscretization(L,N,x,x_grid);
	}
	else
	{
		NonUniformDiscretization(L,N,x,x_grid);
	}
}

void SetPressure(int Nx, int Ny, double** P, double* x, double inlet, double outlet)
{
	double slope = (outlet-inlet)/(x[Nx+1]-x[0]);
	for(int i = 0; i < Nx+2; i++)
	{
		for(int j = 0; j < Ny+2; j++)
		{
			P[i][j] = inlet + slope*(x[i]-x[0]);
		}
	}
}

void SetInitialConditions(int Nx, int Ny, double** A, double top, double bottom, double inlet, double outlet, double mid_region)
{
	for(int i = 0; i < Nx+2; i++)
	{
		for(int j = 0; j < Ny+2; j++)
		{

			A[i][j] = mid_region;

			if(i == 0 || i == 1)
			{
				A[i][j] = inlet;
			}
			else if(i == Nx+1)
			{
				A[i][j] = outlet;
			}

			if(j == Ny+1)
			{
				A[i][j] = top;
			}
			else if (j == 0)
			{
				A[i][j] = bottom;
			}
		}
	}
}

void FillCylinder(cylinder& C, int Nx, int Ny, double Lx, double Ly, double* x, double* y, double ratio)
{

	C.size = Lx/ratio;
	C.center_x = Lx/4;
	C.center_y = Ly/2;


	C.x0 = C.center_x - 0.5*C.size;
	C.x1 = C.center_x + 0.5*C.size;

	C.y0 = C.center_y - 0.5*C.size;
	C.y1 = C.center_y + 0.5*C.size;


	for(int i = 1; i < Nx+1; i++)
	{
		if(x[i]<C.x0 && x[i+1]>=C.x0)
		{
			C.i0 = i;
		}
		else if(x[i]>C.x1 && x[i-1]<=C.x1)
		{
			C.i1 = i;
		}
	}

	for(int j = 1; j < Ny+1; j++)
	{

		if(y[j]<C.y0 && y[j+1]>=C.y0)
		{
			C.j0 = j;
			cout << j << endl;
		}
		else if(y[j]>C.y1 && y[j-1]<=C.y1)
		{
			C.j1 = j;
			cout << j << endl;
		}

	}

}
