double CalculateR(int i, int j, int Ny, cylinder C, double** u, mass_flow** m_dot, double* x_grid, double* y_grid, double* x, double* y, double Re, double inlet, bool direction)
{
	double xstep = StepCalculation(i,x_grid);
	double ystep = StepCalculation(j,y_grid);

	double dxE = x[i+1]-x[i];
	double dxW = x[i]-x[i-1];
	double dyN = y[j+1]-y[j];
	double dyS = y[j]-y[j-1];

	double ue,uw,un,us;

	uw = 0.5*(u[i][j]+u[i-1][j]);
	ue = 0.5*(u[i+1][j]+u[i][j]);
	if(i == 1 && direction)
	{
		uw = inlet;
	}

	if(j>C.j0 && j<C.j1 && direction)
	{
		if(i == C.i0)
		{
			ue = 0.0;
		}
		else if(i == C.i1)
		{
			uw = 0.0;
		}
	}

	un = 0.5*(u[i][j]+u[i][j+1]);
	us = 0.5*(u[i][j]+u[i][j-1]);

	if(!direction)
	{
		if(j == 1)
		{
			us = 0.0;
		}
		else if(j == Ny)
		{
			un = 0.0;
		}

		if(i>C.i0 && i<C.i1)
		{
			if(j == C.j0)
			{
				un = 0.0;
			}
			else if(j == C.j1)
			{
				us = 0.0;
			}
		}
	}

	double de = 1/Re*(u[i+1][j]-u[i][j])/dxE*ystep;
	double dw = 1/Re*(u[i-1][j]-u[i][j])/dxW*ystep;
	double dn = 1/Re*(u[i][j+1]-u[i][j])/dyN*xstep;
	double ds = 1/Re*(u[i][j-1]-u[i][j])/dyS*xstep;


	return -(m_dot[i][j].me*ue + m_dot[i][j].mw*uw + m_dot[i][j].mn*un + m_dot[i][j].ms*us)+de+dw+dn+ds;
}

void CalculatePredictor(int Nx, int Ny, cylinder C, double timestep, double** up, double** u, double** R_old, mass_flow** m_dot, double Re, double* x, double* y, double* x_grid, double* y_grid, double inlet,bool direction)
{
	double R;
	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			if(i>C.i0 && i<C.i1 && j>C.j0 && j<C.j1)
			{
				up[i][j] = 0.0;
			}
			else
			{
				R = CalculateR(i,j,Ny,C,u,m_dot,x_grid,y_grid,x,y,Re,inlet,direction);
				up[i][j] = u[i][j] + timestep*(1.5*R - 0.5*R_old[i][j]);
				R_old[i][j] = R;
			}
		}
	}
}

double ConvectiveBoundaryCondition(int Nx, int j, double* x, double timestep, double** u)
{
	double dx = x[Nx+1]-x[Nx];
	double num = u[Nx+1][j]/timestep/0.8 + u[Nx][j]/dx;
	double den = 1/timestep/0.8+1/dx;

	return num/den;
}

double CorrectVelocity(int Nx, int Ny, cylinder C, double timestep, double* x, double* y, double** p, double** up, double** vp, double** u, double** v)
{
	double old_u, old_v;
	double error_time = 0.0;
	double du, dv;
	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			if(i>C.i0 && i<C.i1 && j>C.j0 && j<C.j1)
			{
				//cout << "I am in" << endl;
				u[i][j] = 0.0;
				v[i][j] = 0.0;
			}
			else
			{
				old_u = u[i][j];
				old_v = v[i][j];
				u[i][j] = up[i][j] - timestep*Gradient(i,j,p,x,true);
				v[i][j] = vp[i][j] - timestep*Gradient(i,j,p,y,false);

				du = u[i][j] - old_u;
				dv = v[i][j] - old_v;

				error_time += du*du + dv*dv;
			}
		}
	}

	for(int j = 0; j < Ny+2; j++)
	{
		//u[Nx+1][j] = u[Nx][j];
		u[Nx+1][j] = ConvectiveBoundaryCondition(Nx,j,x,timestep,u);
		v[Nx+1][j] = v[Nx][j];
	}

	return sqrt(error_time);
}

double FractionalStepMethod(int Nx, int Ny, cylinder C, double Re, double timestep, double pressure_tolerance, double** u, double** v, double** up, double** vp, double** p, double pressure_inlet, double pressure_outlet, double velocity_inlet, double** Ru, double** Rv, mass_flow** m_dot, disc_coefficients** Pres, double* x, double* y, double* x_grid, double* y_grid)
{

	CalculatePredictor(Nx,Ny,C,timestep,up,u,Ru,m_dot,Re,x,y,x_grid,y_grid,velocity_inlet,true);
	CalculatePredictor(Nx,Ny,C,timestep,vp,v,Rv,m_dot,Re,x,y,x_grid,y_grid,0.0,false);

	PressureIndependentTerms(Nx,Ny,C,x_grid,y_grid,up,vp,timestep,Pres,pressure_inlet,pressure_outlet);

	ConjugateGradientMethod(Nx+2,Ny+2,Pres,p,p,pressure_tolerance);

	SetBoundariesPressure(Nx,Ny,p,pressure_outlet);

	return CorrectVelocity(Nx,Ny,C,timestep,x,y,p,up,vp,u,v);
}


void MassFlowCalculations(int Nx, int Ny, cylinder C, double** u, double** v, double** p, double* x, double* y, double* x_grid, double* y_grid, double timestep, mass_flow** m_dot, double inlet)
{
	double xstep, ystep;
	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			xstep = StepCalculation(i,x_grid);
			ystep = StepCalculation(j,y_grid);

			m_dot[i][j].me = 0.5*(u[i][j]+u[i+1][j])*ystep + timestep*(Gradient(i,j,p,x,true)-(p[i+1][j]-p[i][j])/(x[i+1]-x[i]))*ystep;
			m_dot[i][j].mw = -0.5*(u[i][j]+u[i-1][j])*ystep + timestep*(-Gradient(i,j,p,x,true)-(p[i-1][j]-p[i][j])/(x[i-1]-x[i]))*ystep;

			if(i == 1)
			{
				m_dot[i][j].mw = -0.5*inlet*ystep + timestep*(-Gradient(i,j,p,x,true)-(p[i-1][j]-p[i][j])/(x[i-1]-x[i]))*ystep;
			}

			if(j>C.j0 && j<C.j1)
			{
				if(i == C.i0)
				{
					m_dot[i][j].me = 0.0;
				}
				else if(i == C.i1)
				{
					m_dot[i][j].mw = 0.0;
				}
			}


			if(j == 1)
			{
				m_dot[i][j].mn = 0.5*(v[i][j]+v[i][j+1])*xstep + timestep*(-Gradient(i,j,p,y,false)-(p[i][j-1]-p[i][j])/(y[j-1]-y[j]))*xstep;
				m_dot[i][j].ms = 0.0;
			}
			else if(j == Ny)
			{
				m_dot[i][j].mn = 0.0;
				m_dot[i][j].ms = -0.5*(v[i][j]+v[i][j-1])*xstep + timestep*(-Gradient(i,j,p,y,false)-(p[i][j-1]-p[i][j])/(y[j-1]-y[j]))*xstep;

			}
			else
			{
				m_dot[i][j].mn = 0.5*(v[i][j]+v[i][j+1])*xstep + timestep*(Gradient(i,j,p,y,false)-(p[i][j+1]-p[i][j])/(y[j+1]-y[j]))*xstep;
				m_dot[i][j].ms = -0.5*(v[i][j]+v[i][j-1])*xstep + timestep*(-Gradient(i,j,p,y,false)-(p[i][j-1]-p[i][j])/(y[j-1]-y[j]))*xstep;
			}

			if(i>C.i0 && i<C.i1)
			{
				if(j == C.j0)
				{
					m_dot[i][j].mn = 0.0;
				}
				else if(j == C.j1)
				{
					m_dot[i][j].ms = 0.0;
				}
			}
		}
	}
}
