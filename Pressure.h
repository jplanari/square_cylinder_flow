void PressureMatrixCoefficients(int Nx, int Ny, double* x, double* y, double* x_grid, double* y_grid, disc_coefficients** Pres)
{
	double xstep, ystep;
	double dxE, dxW, dyN, dyS;

	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{
			xstep = StepCalculation(i,x_grid);
			ystep = StepCalculation(j,y_grid);

			dxE = x[i+1]-x[i];
			dxW = x[i]-x[i-1];
			dyN = y[j+1]-y[j];
			dyS = y[j]-y[j-1];

			Pres[i][j].aE = ystep/dxE;
			Pres[i][j].aW = ystep/dxW;
			Pres[i][j].aN = xstep/dyN;
			Pres[i][j].aS = xstep/dyS;

			if(j == 1)
			{
				Pres[i][j].aS = 0.0;
			}
			else if(j == Ny)
			{
				Pres[i][j].aN = 0.0;
			}



			Pres[i][j].aP = Pres[i][j].aE + Pres[i][j].aW + Pres[i][j].aN + Pres[i][j].aS;

		}
	}
}

void PressureIndependentTerms(int Nx, int Ny, cylinder C, double* x_grid, double* y_grid, double** up, double** vp, double timestep, disc_coefficients** Pres, double pressure_inlet, double pressure_outlet)
{
	double uw_p, ue_p, vn_p, vs_p;
	double xstep, ystep;
	for(int i = 1; i < Nx+1; i++)
	{
		for(int j = 1; j < Ny+1; j++)
		{

			if(i>C.i0 && i<C.i1 && j>C.j0 && j<C.j1)
			{
				Pres[i][j].bP = 0.0;
			}
			else
			{
				xstep = StepCalculation(i,x_grid);
				ystep = StepCalculation(j,y_grid);

				uw_p = 0.5*(up[i][j]+up[i-1][j]);
				ue_p = 0.5*(up[i][j]+up[i+1][j]);

				if(i == 1)
				{
					uw_p = 1.0;
				}

				if(j > C.j0 && j<C.j1)
				{
					if(i == C.i0)
					{
						ue_p = 0.0;
					}
					else if(i == C.i1)
					{
						uw_p = 0.0;
					}
				}

				vn_p = 0.5*(vp[i][j]+vp[i][j+1]);
				vs_p = 0.5*(vp[i][j]+vp[i][j-1]);

				if(j == 1)
				{
					vs_p = 0.0;
				}
				else if(j == Ny)
				{
					vn_p = 0.0;
				}

				if(i > C.i0 && i < C.i1)
				{
					if(j == C.j0)
					{
						vn_p = 0.0;
					}
					else if(j == C.j1)
					{
						vs_p = 0.0;
					}
				}

				Pres[i][j].bP = 1/timestep*(ystep*(uw_p-ue_p) + xstep*(vs_p-vn_p));

				if(i == Nx)
				{
					Pres[i][j].bP += Pres[i][j].aE*pressure_outlet;
				}
			}
		}
	}
}

void SetBoundariesPressure(int Nx, int Ny, double** p, double pressure_outlet)
{
	for(int i = 1; i < Nx+1; i++)
	{
		p[i][0] = p[i][1];
		p[i][Ny+1] = p[i][Ny];
	}
	for(int j = 1; j < Ny+1; j++)
	{
		p[0][j] = p[1][j];
		p[Nx+1][j] = p[Nx][j];
	}
}
